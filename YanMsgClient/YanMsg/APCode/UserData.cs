﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CSharpWin;
using YanMsg.Properties;
using YanMsg.View;
using Microsoft.Windows.Shell;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows;
using System.Windows.Threading;
using System.Data;

namespace YanMsg.APCode
{
    class UserData
    {

        /// <summary>
        /// 获取应用程序目录
        /// </summary>
        public static String AppDirectory = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase;




        ///// <summary>
        ///// 创建主窗体实例
        ///// </summary>
        public static MainWindow MainWindow;

        /// <summary>
        /// 用户登陆号码
        /// </summary>
        public static string UserNumber { get; set; }

        /// <summary>
        /// 用户真实姓名
        /// </summary>
        public static string UserRealNumber { get; set; }
        //        Image image=new Image();
        //image.Source=new BitmapImage(new Uri(files[i].FullName));

        public static string MySkin = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "Config\\";



        /// <summary>
        /// 在线用户信息
        /// </summary>
        public static List<vw_Yan_Chat_user> ListPeople;


        /// <summary>
        /// 用户讨论组信息
        /// </summary>
        public static ObservableCollection<YanChat_Group> ListGroup = new ObservableCollection<YanChat_Group>();

        /// <summary>
        /// 用户聊天窗口
        /// </summary>
        public static Dictionary<string, Chat> DictionaryChat = new Dictionary<string, Chat>();


        /// <summary>
        /// 讨论组聊天窗口
        /// </summary>
        public static Dictionary<string, GroupChat> DictionaryGroupChat = new Dictionary<string, GroupChat>();


        /// <summary>
        /// 监听接收文件
        /// </summary>
        public static UdpReceiveFile udpReceiveFile;

        /// <summary>
        /// 创建托盘区图标菜单项
        /// </summary>
        public static System.Windows.Forms.MenuItem[] menuItems;
        /// <summary>
        /// 创建托盘区图标实例
        /// </summary>
        public static System.Windows.Forms.NotifyIcon notifyIcon = new System.Windows.Forms.NotifyIcon();

        /// <summary>
        /// 创建托盘区图标右键菜单实例
        /// </summary>
        public static System.Windows.Forms.ContextMenu notifyIconMenu = new System.Windows.Forms.ContextMenu();


        public static string strIsFlagFlash = "N";

        /// <summary>
        /// 未登陆时的托盘区图标
        /// </summary>
        public static System.Drawing.Icon Icon_Login = Resources.Login;

        /// <summary>
        /// 在线时的托盘区图标
        /// </summary>
        public static System.Drawing.Icon Icon_Online = Resources.Online;

        ///// <summary>
        ///// 隐身时的托盘区图标
        ///// </summary>new System.Drawing.Icon(AppDirectory + "\\Img\\Online.ico");
        //public static System.Drawing.Icon Icon_Hide = new System.Drawing.Icon(AppDirectory + "\\Icon\\Hide.ico");

        ///// <summary>
        ///// 忙碌时的托盘区图标
        ///// </summary>
        //public static System.Drawing.Icon Icon_Busyness = new System.Drawing.Icon(AppDirectory + "\\Icon\\Busyness.ico");

        ///// <summary>
        ///// 请勿打扰时的托盘区图标
        ///// </summary>
        //public static System.Drawing.Icon Icon_Not = new System.Drawing.Icon(AppDirectory + "\\Icon\\No.ico");

        /// <summary>
        /// 离线时的托盘区图标
        /// </summary>
        public static System.Drawing.Icon Icon_Offline = Resources.Off;
        public static Icon touminico = new Icon(Resources.toumin, new System.Drawing.Size(32, 32));
        public static Icon Msgico = new Icon(Resources.Msg, new System.Drawing.Size(32, 32));

        //出差休假信息
        public static DataTable dtXJinfo = new DataTable();



        public static Icon ToIco(Bitmap bit)
        {
            Bitmap bit2 = bit.Clone(new Rectangle(0, 0, 32, 32), System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            Icon icon = System.Drawing.Icon.FromHandle(bit2.GetHicon());
            return icon;
        }


    }
}
