﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using System.Xml.XPath;

namespace YanMsg.APCode
{
    /// <summary>
    /// Provides an attached property determining the current Bing image and assigning it to an image or imagebrush.
    /// </summary>
    public static class BingImage
    {
        public static readonly DependencyProperty UseBingImageProperty = DependencyProperty.RegisterAttached("UseBingImage", typeof(bool), typeof(BingImage), new PropertyMetadata(OnUseBingImageChanged));

        public static string strBGName = ConfigManger.ClientUserBG;

        private static BitmapImage cachedBingImage;

        private static void OnUseBingImageChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var newValue = (bool)e.NewValue;
            var image = o as Image;
            var imageBrush = o as ImageBrush;

            if (!newValue || (image == null && imageBrush == null)) {
                return;
            }


            var url = GetCurrentBingImageUrl(strBGName);

            cachedBingImage = new BitmapImage(url);

            if (cachedBingImage != null){
                if (image != null) {
                    image.Source = cachedBingImage;
                }
                else if (imageBrush != null) {
                    imageBrush.ImageSource = cachedBingImage;
                }
            }
        }

        private static  Uri GetCurrentBingImageUrl(string strJpgName)
        {
            return new Uri(UserData.MySkin + strJpgName, UriKind.Absolute);
        }


        public static bool GetUseBingImage(DependencyObject o)
        {
            return (bool)o.GetValue(UseBingImageProperty);
        }

        public static void SetUseBingImage(DependencyObject o, bool value)
        {
            o.SetValue(UseBingImageProperty, value);
        }
    }
}
