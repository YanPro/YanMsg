﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using agsXMPP;

namespace YanMsg.APCode
{
    public static class MyExtensions
    {
        public static int[] SplitTOInt(this string strs, char ch)
        {
            string[] arrstr = strs.Split(ch);
            int[] arrint = new int[arrstr.Length];
            for (int i = 0; i < arrstr.Length; i++)
            {
                arrint[i] = int.Parse(arrstr[i].ToString());
            }
            return arrint;
        }

        public static Jid TOJid(this string strs)
        {
            return new Jid(strs, ConfigManger.ServerIP, strs);
        }
        public static List<string> SplitTOList(this string strs, char ch)
        {
            List<string> Lstr = new List<string>();
            string[] arrstr = strs.Split(ch);
            int[] arrint = new int[arrstr.Length];
            for (int i = 0; i < arrstr.Length; i++)
            {
                Lstr.Add(arrstr[i].ToString());
            }
            return Lstr;
        }

        public static Dictionary<string, string> SplitTODictionary(this string strs, char ch, string strKey)
        {
            Dictionary<string, string> Lstr = new Dictionary<string, string>();
            string[] arrstr = strs.Split(ch);
            int[] arrint = new int[arrstr.Length];
            for (int i = 0; i < arrstr.Length; i++)
            {
                Lstr.Add(arrstr[i].ToString(), strKey);
            }
            return Lstr;
        }

        /// <summary>
        /// 将List转化为String
        /// </summary>
        /// <param name="Lists"></param>
        /// <param name="ch"></param>
        /// <returns></returns>
        public static string ListTOString(this List<string> Lists, char ch)
        {
            string strReturn = "";
            foreach (var item in Lists)
            {
                strReturn = strReturn + item.ToString() + ch;
            }
            return strReturn;
        }
        /// <summary>
        /// 获取需要IN的格式
        /// </summary>
        /// <param name="strKys"></param>
        /// <returns></returns>
        public static string ToFormatLike(this string strKys)
        {
            StringBuilder sbKeys = new StringBuilder();
            foreach (var item in strKys.Split(','))
            {
                sbKeys.AppendFormat("'" + item.ToString() + "',");
            }
            return sbKeys.Length > 0 ? sbKeys.ToString().TrimEnd(',').Trim('\'') : "";
        }

        public static string ToMenuFormat(this string strMenu)
        {
            string strReturn = "";
            if (strMenu.Length == 1)
            {
                strReturn = "0" + strMenu.ToString();
            }
            else
            {
                strReturn = strMenu.ToString();
            }
            return strReturn;
        }


        //List转ObservableCollection
        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> enumerableList)
        {
            if (enumerableList != null)
            {
                //create an emtpy observable collection object  
                var observableCollection = new ObservableCollection<T>();

                //loop through all the records and add to observable collection object  
                foreach (T item in enumerableList)
                    observableCollection.Add(item);

                //return the populated observable collection  
                return observableCollection;
            }
            return null;
        }

        //ObservableCollection 排序
        public static void Sort<T>(this ObservableCollection<T> Collection)
        {
            Collection.Sort(Comparer<T>.Default);
        }
        //ObservableCollection 排序
        public static void Sort<T>(this ObservableCollection<T> Collection, IComparer<T> comparer)
        {
            int i, j;
            T index;
            for (i = 1; i < Collection.Count; i++)
            {
                index = Collection[i];
                j = i;
                while ((j > 0) && (comparer.Compare(Collection[j - 1], index) == 1))
                {
                    Collection[j] = Collection[j - 1];
                    j = j - 1;
                }
                Collection[j] = index;
            }
        }
    }

}