﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using agsXMPP;

namespace YanMsg.APCode
{
    public class Yan_UserB : BaseEFDao<vw_Yan_Chat_user>
    {
    }

    public class UG : BaseEFDao<YanChat_UserGroup>
    {
    }

    public class Msg : BaseEFDao<YanChat_Msg>
    {
    }

    public class User : BaseEFDao<Yan_User>
    {
        public DataTable getXJXinXi()
        {
            DataTable dtReturn = new DataTable();
            dtReturn = GetDTByCommand(" SELECT  CRUser   LeiBie, StarTime, EndTime,Pro_ZiDian.TypeName FROM GR_CCXJ  join Pro_ZiDian on GR_CCXJ.LeiBie=Pro_ZiDian.TypeNO WHERE  getdate()  BETWEEN  CAST(StarTime as datetime) AND  CAST(EndTime as datetime) AND Pro_ZiDian.Class=3 ");
            return dtReturn;
        }
    }

    public class YanChat_MsgContentB: BaseEFDao<YanChat_MsgContent>
    {

    }
    public class YanChat_UserMsgB : BaseEFDao<YanChat_UserMsg>
    {

    }
    public class vw_MsgUser : BaseEFDao<vw_Yan_Chat_MsgUser>
    {
    }

    public class YanChat_FileB : BaseEFDao<YanChat_File>
    {

    }

    public class DayLog : BaseEFDao<Yan_DayLog>
    {

    }

    public class TBYanChat_Group : BaseEFDao<YanChat_Group>
    {

    }
    public class YanChat_ServerFileB : BaseEFDao<YanChat_ServerFile>
    {

    }

    public class YanChat_ServerFileTypeB : BaseEFDao<YanChat_ServerFileType>
    {

    }

    //聊天历史记录
    public class YanChat_ChatHistoryB : BaseEFDao<YanChat_ChatHistory>
    {

    }



    //配置表
    public class YanChat_ConfigB : BaseEFDao<YanChat_Config>
    {

    }
}