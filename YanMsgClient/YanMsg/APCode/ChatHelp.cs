﻿using agsXMPP.protocol.client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;

namespace YanMsg.APCode
{



    public class ChatHelp
    {
        public static void delMsg(string strUserName)
        {
            try
            {
                new YanChat_ChatHistoryB().Delete(d => d.cruser == UserData.UserNumber && (d.userfrom == strUserName || d.userto == strUserName));
            }
            catch (Exception ex)
            {

            }

        }



        public static DataTable GetGroupChatUses(int GroupID)
        {
            DataTable dtUser = new DataTable();
            //查询从50条起的20条记录
            try
            {
                string sql = " SELECT distinct CRUser from dbo.YanChat_GroupMsg where GroupID= '" + GroupID + "'";
                dtUser = new YanChat_MsgContentB().GetDTByCommand(sql);
            }
            catch (Exception ex)
            {

            }
            return dtUser;

        }


        public static DataTable GetChatUser(string strUserName)
        {
            DataTable dtUser = new DataTable();
            //查询从50条起的20条记录
            try
            {
                string sql = " select userfrom from (  select distinct userfrom from YanChat_ChatHistory  WHERE cruser='" + UserData.UserNumber + "'   union select distinct userto from YanChat_ChatHistory  WHERE cruser='" + UserData.UserNumber + "'   ) T where   userfrom <> '" + strUserName + "' ";
                dtUser = new YanChat_ChatHistoryB().GetDTByCommand(sql);

            }
            catch (Exception ex)
            {

            }
            return dtUser;
        }

        public static List<YanChat_ChatHistory> GetUserChatHis(string strUserName)
        {
            List<YanChat_ChatHistory> ListChat = new List<YanChat_ChatHistory>();            //查询从50条起的20条记录
            try
            {
                ListChat = new YanChat_ChatHistoryB().GetEntities(d => d.cruser == UserData.UserNumber && (d.userfrom == strUserName || d.userto == strUserName)).ToList();
            }
            catch (Exception ex)
            {

            }
            return ListChat;
        }



        //获取讨论组聊天记录
        public static DataTable GetGroupChatHis(int GroupID, string strUserName = "", int intcount = 500)
        {
            DataTable dtHis = new DataTable();
            //查询从50条起的20条记录
            try
            {
                string sql = "SELECT top " + intcount + " * from dbo.YanChat_GroupMsg where GroupID= '" + GroupID + "'  ";
                if (strUserName != "")
                {
                    sql = sql + " AND CRUser='" + strUserName + "'";
                }
                dtHis = new YanChat_ChatHistoryB().GetDTByCommand(sql);

            }
            catch (Exception ex)
            {

            }
            return dtHis;
        }


        public static void InsertChat(string strUserFrom, string strUserTo, string strMsg, int intChatType = (int)MessageType.chat)
        {
            try
            {
                new YanChat_ChatHistoryB().Insert(new YanChat_ChatHistory { userfrom = strUserFrom, userto = strUserTo, mescontent = strMsg, crdate = DateTime.Now, cruser = UserData.UserNumber, remark = intChatType.ToString() });
            }
            catch (Exception ex)
            {

            }
        }


        public bool Ping(string ip)
        {
            try
            {
                System.Net.NetworkInformation.Ping p = new System.Net.NetworkInformation.Ping();

                System.Net.NetworkInformation.PingOptions options = new System.Net.NetworkInformation.PingOptions();

                options.DontFragment = true;

                string data = "Test Data!";

                byte[] buffer = Encoding.ASCII.GetBytes(data);

                int timeout = 1000; // Timeout 时间，单位：毫秒

                System.Net.NetworkInformation.PingReply reply = p.Send(ip, timeout, buffer, options);

                if (reply.Status == System.Net.NetworkInformation.IPStatus.Success)

                    return true;

                else

                    return false;
            }
            catch (Exception)
            {
                return false;
            }


        }

        public static string GetRepose(string strUrL)
        {
            try
            {
                WebRequest request = WebRequest.Create(strUrL);//为指定的 URI 方案初始化新的 System.Net.WebRequest 实例
                request.UseDefaultCredentials = false;//获取或设置一个 System.Boolean 值，该值控制 System.Net.CredentialCache.DefaultCredentials
                WebResponse response = request.GetResponse();//返回对 Internet 请求的响应。
                System.IO.Stream resStream = response.GetResponseStream();//返回从 Internet 资源返回数据流
                StreamReader sr = new StreamReader(resStream, System.Text.Encoding.UTF8);//实例华一个流的读写器
                string strReturn = sr.ReadToEnd();//这就是百度首页的HTML哦 ,字符串形式的流的其余部分（从当前位置到末尾）。如果当前位置位于流的末尾，则返回空字符串 ("")
                resStream.Close();//关闭当前流并释放与之关联的所有资源
                sr.Close(); //关闭 System.IO.StreamReader 对象和基础流，并释放与读取器关联的所有系统资源
                return strReturn;
            }
            catch (Exception)
            {

                return "";
            }

        }


    }

    public class MouseKeyBoardOperate
    {
        /// <summary>
        /// 创建结构体用于返回捕获时间
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        struct LASTINPUTINFO
        {
            /// <summary>
            /// 设置结构体块容量
            /// </summary>
            [MarshalAs(UnmanagedType.U4)]
            public int cbSize;

            /// <summary>
            /// 抓获的时间
            /// </summary>
            [MarshalAs(UnmanagedType.U4)]
            public uint dwTime;
        }

        [DllImport("user32.dll")]
        private static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);
        /// <summary>
        /// 获取键盘和鼠标没有操作的时间
        /// </summary>
        /// <returns>用户上次使用系统到现在的时间间隔，单位为秒</returns>
        public static long GetLastInputTime()
        {
            LASTINPUTINFO vLastInputInfo = new LASTINPUTINFO();
            vLastInputInfo.cbSize = Marshal.SizeOf(vLastInputInfo);
            if (!GetLastInputInfo(ref vLastInputInfo))
            {
                return 0;
            }
            else
            {
                long count = Environment.TickCount - (long)vLastInputInfo.dwTime;
                long icount = count / 1000;
                return icount;
            }
        }

    }






}
