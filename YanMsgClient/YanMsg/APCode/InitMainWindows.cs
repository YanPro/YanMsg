﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace YanMsg.APCode
{
    internal class InitMainWindow
    {
        /// <summary>
        /// 好友列表
        /// </summary>
        /// <param name="MainWin">主窗体句柄</param>
        //public static void Init(MainList MainWin)
        //{
        //    //用户基本信息
        //    MainWin.UserNickName.Text = UserData.UserNickName + "(" + UserData.UserNumber + ")";
        //    MainWin.DictumBox.Text = UserData.UserStatus;
        //    MainWin.DictumBox.ToolTip = UserData.UserStatus;
        //    MainWin.UserStuts.Text = UserData.UserStatus;
        //    UserData.notifyIcon.Icon = UserData.Icon_Online;
        //    MainWin.AppVersion.Text = "YanChat " + new ConfigManger().CurrentVersion.ToString();
        //    string strUserSkin = ConfigManger.GetConfigInfo("UserSkin");
        //    MainWin.MainBG.Source = new BitmapImage(new Uri(@"pack://application:,,,/Img/BG/" + strUserSkin + ".jpg"));
        //    MainWin.MyTX.Source = new BitmapImage(new Uri(@"pack://application:,,,/Img/TX/" + UserData.UserTX + ".jpg"));
        //    MainWin.MsgCount.Text = "(" + new YanChatHelp().GetUserXiaoXI(UserData.UserNumber) + ")";
        //    Application_Init.maininit();
        //}

        public static void BingAllPeople()
        {
            MainPeopleList.ModifyPeopleTree();
        }

        private static void AddTree_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            TreeViewItem item = sender as TreeViewItem;
            if (item != null)
            {
                item.Focus();
            }
        }

        //private static ContextMenu rightButtonMenu()
        //{
        //    MenuItem sendmsgitem = new MenuItem();
        //    sendmsgitem.Header = "发送消息";
        //    sendmsgitem.Click += new RoutedEventHandler(MainList.sendmsgitem_Click);

        //    ContextMenu contextmenu = new ContextMenu();
        //    contextmenu.Items.Add(sendmsgitem);

        //    return contextmenu;
        //}
    }

    public class TreeUser : INotifyPropertyChanged
    {
        /// <summary>
        /// 用户编号
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserRealName { get; set; }

        /// <summary>
        /// 用户所在部门
        /// </summary>
        public string DeptName { get; set; }

        /// <summary>
        /// 用户签名
        /// </summary>
        private string _UserSign;

        public string UserSign
        {
            get
            {
                return this._UserSign;
            }
            set
            {
                if (this._UserSign != value)
                {
                    this._UserSign = value;
                    OnPropertyChanged("UserSign");
                }
            }
        }

        /// <summary>
        /// 用户在线状态
        /// </summary>
        private string _IsOnline;

        public string IsOnline
        {
            get
            {
                return this._IsOnline;
            }
            set
            {
                if (this._IsOnline != value)
                {
                    this._IsOnline = value;
                    OnPropertyChanged("IsOnline");
                }
            }
        }

        /// <summary>
        /// 用户IP
        /// </summary>
        private string _UserIP;

        public string UserIP
        {
            get
            {
                return this._UserIP;
            }
            set
            {
                if (this._UserIP != value)
                {
                    this._UserIP = value;
                    OnPropertyChanged("UserIP");
                }
            }
        }

        /// <summary>
        /// 用户头像
        /// </summary>
        private string _UserTX;

        public string UserTX
        {
            get
            {
                return this._UserTX;
            }
            set
            {
                if (this._UserTX != value)
                {
                    this._UserTX = value;
                    OnPropertyChanged("UserTX");
                }
            }
        }

        /// <summary>
        /// 用户IP
        /// </summary>
        private bool _isChecked;

        public bool isChecked
        {
            get
            {
                return this._isChecked;
            }
            set
            {
                if (this._isChecked != value)
                {
                    this._isChecked = value;
                    OnPropertyChanged("isChecked");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public class Branch : INotifyPropertyChanged
    {
        /// <summary>
        /// 用户总数
        /// </summary>
        private string _UserCount;

        public string UserCount
        {
            get
            {
                return this._UserCount;
            }
            set
            {
                if (this._UserCount != value)
                {
                    this._UserCount = value;
                    OnPropertyChanged("UserCount");
                }
            }
        }

        /// <summary>
        /// 用户在线状态
        /// </summary>
        private string _OnlineUserCount;

        public string OnlineUserCount
        {
            get
            {
                return this._OnlineUserCount;
            }
            set
            {
                if (this._OnlineUserCount != value)
                {
                    this._OnlineUserCount = value;
                    OnPropertyChanged("OnlineUserCount");
                }
            }
        }

        /// <summary>
        /// 部门名称
        /// </summary>
        // public string TeacherName { get; set; }
        private string _BranchName;

        public string BranchName
        {
            get
            {
                return this._BranchName;
            }
            set
            {
                if (this._BranchName != value)
                {
                    this._BranchName = value;
                    OnPropertyChanged("BranchName");
                }
            }
        }



        

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion INotifyPropertyChanged Members

        /// <summary>
        /// 用户IP
        /// </summary>
        private bool _isChecked;

        public bool isChecked
        {
            get
            {
                return this._isChecked;
            }
            set
            {
                if (this._isChecked != value)
                {
                    this._isChecked = value;
                    OnPropertyChanged("isChecked");
                    this.SetChiCheck(value);
                }
            }
        }

        /// <summary>
        /// 用户列表
        /// </summary>
        public ObservableCollection<TreeUser> TreeUserList { get; set; }

        public void SetChiCheck(bool isCheck)
        {
            foreach (var item in TreeUserList)
            {
                item.isChecked = isCheck;
            }
        }
    }

    public class MainPeopleList
    {
        public static ObservableCollection<Branch> PeopleTree;

        static MainPeopleList()
        {
            PeopleTree = new ObservableCollection<Branch>();
            ObservableCollection<TreeUser> AllUserList = new ObservableCollection<TreeUser>();
            foreach (var item in UserData.ListPeople)
            {
                TreeUser TreeUserN = new TreeUser();
                TreeUserN.UserName = item.UserName;
                TreeUserN.UserSign = item.Usersign;
                TreeUserN.UserIP = item.UserIPAdress;
                TreeUserN.IsOnline = item.ISonline;
                TreeUserN.UserRealName = item.UserRealName;
                TreeUserN.DeptName = item.DeptName;
                TreeUserN.UserTX = item.tx;

                AllUserList.Add(TreeUserN);
            }
            List<string> ListBranch = AllUserList.Where(d => d.UserName != "lyh").Select(d => d.DeptName).Distinct().ToList();
            foreach (string item in ListBranch)
            {
                Branch NewBranch = new Branch();
                NewBranch.BranchName = item.ToString();
                NewBranch.UserCount = AllUserList.Where(d => d.DeptName == item).Count().ToString();
                NewBranch.OnlineUserCount = AllUserList.Where(d => d.DeptName == item&&d.IsOnline == "Y").Count().ToString();

                NewBranch.TreeUserList = AllUserList.Where(d => d.DeptName == item.ToString()).OrderBy(d => d.IsOnline).ToObservableCollection();
                MainPeopleList.PeopleTree.Add(NewBranch);
            }
        }

        public static void ModifyPeopleTree()
        {
            foreach (Branch BranchTree in PeopleTree)
            {
                string strOnlineUserCount = UserData.ListPeople.Where(d => d.DeptName == BranchTree.BranchName && d.ISonline == "Y").Count().ToString();
                if (strOnlineUserCount != BranchTree.OnlineUserCount)
                {
                    BranchTree.OnlineUserCount = strOnlineUserCount;
                }

                foreach (TreeUser treeUser in BranchTree.TreeUserList)
                {
                    vw_Yan_Chat_user User = UserData.ListPeople.Where(d => d.UserName == treeUser.UserName).SingleOrDefault();
                    if (treeUser.IsOnline != User.ISonline)
                    {
                        treeUser.IsOnline = User.ISonline;
                    }
                    if (treeUser.UserSign != User.Usersign)
                    {
                        treeUser.UserSign = User.Usersign;
                    }
                    if (treeUser.UserIP != User.UserIPAdress)
                    {
                        treeUser.UserIP = User.UserIPAdress;
                    }
                    if (treeUser.UserTX != User.tx)
                    {
                        treeUser.UserTX = User.tx;
                    }
                }
                BranchTree.TreeUserList.Sort(new IsLineCompare());



            }
        }

        class IsLineCompare : IComparer<TreeUser>
        {
            public int Compare(TreeUser a, TreeUser b)
            {
                return b.IsOnline.CompareTo(a.IsOnline);
            }
        }  

        private static int Comparison(TreeUser student1, TreeUser student2)
        {

            return string.Compare(student2.IsOnline, student1.IsOnline);
        }

        //public static void BingAllPeople(TreeView TreeViewPeople, List<vw_Yan_Chat_user> allPeople)
        //{
        //    TreeViewPeople.Items.Clear();
        //    TreeViewItem QYItem = new TreeViewItem();
        //    QYItem.Header = "企业架构(" + allPeople.Where(d => d.ISonline == "Y").Count().ToString() + "/" + allPeople.Count().ToString() + ")";
        //    QYItem.FontSize = 16;
        //    QYItem.IsExpanded = true;
        //    TreeViewPeople.Items.Add(QYItem);
        //    List<string> Branch = allPeople.Select(d => d.DeptName).Distinct().ToList();
        //    foreach (string item in Branch)
        //    {
        //        string PeopCount = allPeople.Where(d => d.DeptName == item).Count().ToString();
        //        string PeopisLineCount = allPeople.Where(d => d.DeptName == item && d.ISonline == "Y").Count().ToString();

        //        TreeViewItem BranchItem = new TreeViewItem();
        //        BranchItem.Header = item.ToString() + "(" + PeopisLineCount + "/" + PeopCount + ")";
        //        BranchItem.FontSize = 16;
        //        QYItem.Items.Add(BranchItem);
        //    }
        //    foreach (TreeViewItem item in QYItem.Items)
        //    {
        //        string strDeptName = item.Header.ToString().Substring(0, item.Header.ToString().IndexOf('('));
        //        List<vw_Yan_Chat_user> ListPeople = allPeople.Where(d => d.DeptName == strDeptName).ToList();

        //        foreach (var people in ListPeople)
        //        {
        //            TreeViewItem PeopleItem = new TreeViewItem();
        //            StackPanel Panel = new StackPanel();
        //            Panel.Orientation = Orientation.Horizontal;
        //            Panel.Height = 55;
        //            Panel.Width = 120;

        //            Image userimg = new Image();//用户头像
        //            userimg.Height = 50;
        //            userimg.Width = 50;
        //            userimg.Source = new BitmapImage(new Uri(@"pack://application:,,,/Img/TX/" + "TX3.jpg"));

        //            if (people.ISonline != "Y")
        //            {
        //                FormatConvertedBitmap fcb = new FormatConvertedBitmap((BitmapSource)userimg.Source, PixelFormats.Gray32Float, null, 0);
        //                userimg.Source = fcb;
        //            }

        //            Border imgBorder = new Border();
        //            imgBorder.Child = userimg;
        //            imgBorder.Style = (System.Windows.Style)Application.Current.Resources["TalkBorder"];
        //            imgBorder.Margin = new Thickness(-6, 0, 0, 0);
        //            imgBorder.Width = 55;
        //            imgBorder.Height = 55;
        //            Panel.Children.Add(imgBorder);

        //            TextBlock TBUserID = new TextBlock();
        //            TBUserID.Text = people.UserName;
        //            TBUserID.FontSize = 12;
        //            TBUserID.Visibility = Visibility.Collapsed;
        //            Panel.Children.Add(TBUserID);

        //            StackPanel FontPanel = new StackPanel();
        //            FontPanel.Orientation = Orientation.Vertical;
        //            FontPanel.Height = 40;
        //            FontPanel.Width = 100;

        //            TextBlock TBUserName = new TextBlock();
        //            TBUserName.Text = people.UserRealName;
        //            TBUserName.Margin = new Thickness(2, -4, 0, 0);
        //            TBUserName.Foreground = new SolidColorBrush(Colors.Black);

        //            FontPanel.Children.Add(TBUserName);

        //            TextBlock TBUserStatus = new TextBlock();
        //            TBUserStatus.Text = people.Usersign;
        //            TBUserStatus.ToolTip = people.Usersign;
        //            TBUserStatus.Margin = new Thickness(2, 6, 0, 0);

        //            TBUserStatus.FontSize = 14;
        //            TBUserStatus.Foreground = new SolidColorBrush(Colors.Gray);
        //            FontPanel.Children.Add(TBUserStatus);
        //            Panel.Children.Add(FontPanel);
        //            Panel.Margin = new Thickness(2, 0, 0, 1);
        //            Panel.Width = 170;

        //            PeopleItem.Header = Panel;
        //            PeopleItem.Margin = new Thickness(-20, 5, 0, 1);

        //            PeopleItem.Style = (System.Windows.Style)Application.Current.Resources["PeopleTreeItem"];

        //            PeopleItem.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(MainList.AddTree_MouseDoubleClick);
        //            PeopleItem.MouseRightButtonUp += new System.Windows.Input.MouseButtonEventHandler(AddTree_MouseRightButtonUp);
        //            PeopleItem.ContextMenu = rightButtonMenu();
        //            item.Items.Add(PeopleItem);
        //        }
        //    }
        //}
    }
}