﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Microsoft.Win32;
using System.Net.NetworkInformation;
using System.Data;

namespace YanMsg.APCode
{
    public class ConfigManger
    {
        public static string CurrentVersion = "";

        public static string PCName = "";
        //服务器端口
        public static string ServerIP = "";
        //服务器端口
        public static string ServerPort = "";

        //客户端默认接收地址
        public static string ServerRevDefaultPah = "";

        //自动更新地址
        public static string ClientUpdatePath = "";

        //默认背景图片
        public static string ClientUserBG = "";

        //默认皮肤
        public static string ClientUserSkin = "";

        //默认文件上传路径
        public static string ClientFileCenterUrl = "";

        //默认头像
        public static string ClientUserTX = "";

        //网站登录地址
        public static string ClientWebUrl = "";

        //默认登录账号
        public static string ClientUserName = "";

        //默认登录密码
        public static string ClientPassword = "";

        //默认是否自动登录
        public static string ClientisAutoRun = "";

        //默认是否开机启动
        public static string ClientisStartUp = "";

        //默认是否断线后重新登录
        public static string ServerAutoLogin = "";
        public void initData()
        {
            CurrentVersion = GetVersion();
            PCName = GetPCName() + GetMacAddress();
            DataTable dt = GetConfigFormDB();
            ServerIP = GetConfigFormDT(dt, "ServerIP");
            ServerPort = GetConfigFormDT(dt, "ServerPort");
            ServerRevDefaultPah = GetConfigFormDT(dt, "ServerRevDefaultPah");
            ServerAutoLogin = GetConfigFormDT(dt, "ServerAutoLogin");
            ClientUpdatePath = GetConfigFormDT(dt, "ClientUpdatePath");
            ClientUserBG = GetConfigFormDT(dt, "ClientUserBG");
            ClientUserSkin = GetConfigFormDT(dt, "ClientUserSkin");
            ClientFileCenterUrl = GetConfigFormDT(dt, "ClientFileCenterUrl");
            ClientUserTX = GetConfigFormDT(dt, "ClientUserTX");
            ClientWebUrl = GetConfigFormDT(dt, "ClientWebUrl");
            ClientUserName = GetConfigFormDT(dt, "ClientUserName");
            ClientPassword = GetConfigFormDT(dt, "ClientPassword");
            ClientisAutoRun = GetConfigFormDT(dt, "ClientisAutoRun");
            ClientisStartUp = GetConfigFormDT(dt, "ClientisStartUp");

        }

        public string GetVersion()
        {
            try
            {
                return new Version(System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetEntryAssembly().Location).ProductVersion).ToString();
            }
            catch (Exception)
            {
                return "2.16";
            }
        }

        public DataTable GetConfigFormDB()
        {
            DataTable dtReturn = new YanChat_ConfigB().GetDTByCommand("select [key] , [value], keyowner from YanChat_Config where keyowner='YanChat' union all select [key] , [value], keyowner from YanChat_Config where keyowner='" + ConfigManger.PCName + "'");
            return dtReturn;
        }
        public string GetConfigFormDT(DataTable dt, string strConfigKey)
        {
            string strReturn = "";
            DataRow[] datarows = dt.Select("key= '" + strConfigKey + "' and keyowner='" + ConfigManger.PCName + "'");
            if (datarows.Length == 0)
            {
                datarows = dt.Select("key= '" + strConfigKey + "' and keyowner='YanChat'");
            }
            if (datarows.Length == 0)
            {
                return strReturn;
            }
            else
            {

                return datarows[0].ItemArray[1].ToString();
            }
        }
        public string GetConfig(string strKey)
        {
            string strReturn = "";
            YanChat_Config model = new YanChat_Config();
            model = new YanChat_ConfigB().GetEntity(d => d.keyowner == ConfigManger.PCName && d.key == strKey);
            if (model == null)
            {
                model = new YanChat_ConfigB().GetEntity(d => d.keyowner == "YanChat" && d.key == strKey);
            }
            if (model != null)
            {
                strReturn = model.value;
            }
            return strReturn;
        }

        public string GetPCName()
        {
            try
            {
                return System.Net.Dns.GetHostName();
            }
            catch (Exception)
            {

                return "";
            }
        }
        //获取Mac    
        /// <summary>  
        /// 通过网络适配器获取MAC地址  
        /// </summary>  
        /// <returns></returns>  
        public string GetMacAddress()
        {
            string macAddress = "";
            try
            {
                NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
                foreach (NetworkInterface adapter in nics)
                {
                    if (!adapter.GetPhysicalAddress().ToString().Equals(""))
                    {
                        macAddress = adapter.GetPhysicalAddress().ToString();
                        for (int i = 1; i < 6; i++)
                        {
                            macAddress = macAddress.Insert(3 * i - 1, ":");
                        }
                        break;
                    }
                }

            }
            catch
            {
            }
            return macAddress;
        }
        //设置用户配置
        public void SetConfig(string strKey, string strValue)
        {
            YanChat_Config model = new YanChat_ConfigB().GetEntity(d => d.keyowner == ConfigManger.PCName && d.key == strKey);
            if (model == null)
            {
                new YanChat_ConfigB().Insert(new YanChat_Config { key = strKey, value = strValue, keyowner = ConfigManger.PCName, crdate = DateTime.Now });
            }
            else
            {
                model.value = strValue;
                new YanChat_ConfigB().Update(model);
            }
        }


        //删除用户配置
        public void DelConfig(string strKey)
        {
            new YanChat_ConfigB().Delete(d => d.keyowner == ConfigManger.PCName && d.key == strKey);
        }


        //设置开机自启动
        public bool SetAutoRun(string keyName, string filePath)
        {
            try
            {
                RegistryKey runKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

                if (runKey == null)
                {
                    runKey = Registry.LocalMachine.CreateSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run");
                }
                runKey.SetValue(keyName, filePath);

                runKey.Close();
            }

            catch
            {
                return false;
            }

            return true;
        }

        //去除开机自启动
        public bool DeleteAutoRun(string keyName)
        {
            try
            {
                RegistryKey runKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

                runKey.DeleteValue(keyName, false);

                runKey.Close();
            }

            catch
            {
                return false;
            }

            return true;
        }

    }


    public class YanCommand
    {
        public static string UpdateGroup = "UpdateGroup";

    }


}