﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FirstFloor.ModernUI.Windows.Controls;
using YanMsg.APCode;

namespace YanMsg.View.Sys.UC
{
    /// <summary>
    /// SetPasd.xaml 的交互逻辑
    /// </summary>
    public partial class SetPasd : UserControl
    {
        public SetPasd()
        {
            InitializeComponent();
        }

        private void btUpdatePad_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UserData.ListPeople.SingleOrDefault(d => d.UserName == UserData.UserNumber).UserPass != txtOldpasd.Password)
                {
                    MsgManger.Show("输入的旧密码错误");
                    return;
                }
                if (txtNewpasd.Password != txtNewpasd1.Password)
                {
                    MsgManger.Show("两次输入的新密码不一致");
                    return;
                }
                if (txtNewpasd.Password.Trim() == "")
                {
                    MsgManger.Show("密码不能为空");
                    return;
                }
                new YanChatHelp().UpdatePad(UserData.UserNumber, txtNewpasd.Password);
                btUpdatePad.Content = "修改成功";
            }
            catch (Exception)
            {
                btUpdatePad.Content = "修改失败";
            }
            btUpdatePad.IsEnabled = false;

          
        }

        private void btReSetPad_Click(object sender, RoutedEventArgs e)
        {
            try
            {
               var result = ModernDialog.ShowMessage("确认要重置密码吗?", "提示", MessageBoxButton.YesNo);
                //确定后删除该消息
               if (result.ToString() == "Yes")
               {
                   new YanChatHelp().UpdatePad(UserData.UserNumber, "123");
                   btReSetPad.Content = "修改成功";
               }
            }
            catch (Exception)
            {
                btReSetPad.Content = "修改失败";
            }
            btReSetPad.IsEnabled = false;

        }
    }
}