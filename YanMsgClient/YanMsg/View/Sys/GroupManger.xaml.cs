﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using YanMsg.APCode;
using System.Threading;
using FirstFloor.ModernUI.Windows.Controls;
using System.Threading.Tasks;
using agsXMPP.protocol.client;

namespace YanMsg.View
{
    /// <summary>
    /// GetTX.xaml 的交互逻辑
    /// </summary>
    public partial class GroupManger : ModernWindow
    {
        public GroupManger()
        {
            InitializeComponent();
            this.Style = (Style)App.Current.Resources["EmptyWindow"];
            RootItem.ItemsSource = MainPeopleList.PeopleTree;

            ;
        }


        private void UpdateTask()
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                YanChat_Group NGroup = new YanChat_Group { GroupName = conMsgTitle.Text, GroupCreateUser = UserData.UserNumber, GroupCreateDate = DateTime.Now.ToShortDateString(), GroupDesc = "" };
                UserData.ListGroup.Add(NGroup);
                new TBYanChat_Group().Insert(NGroup);

                //添加用户
                foreach (var item in MainPeopleList.PeopleTree)
                {
                    foreach (TreeUser user in item.TreeUserList)
                    {
                        if (user.isChecked && user.UserName != UserData.UserNumber)
                        {
                            new UG().Insert(new YanChat_UserGroup { UserGroupID = NGroup.ID.ToString(), UserID = user.UserName });
                        }
                    }
                }
                new UG().Insert(new YanChat_UserGroup { UserGroupID = NGroup.ID.ToString(), UserID = UserData.UserNumber });


                Message msg = new Message();
                msg.Type = MessageType.headline;
                msg.From = UserData.UserNumber.TOJid();
                msg.Body = YanCommand.UpdateGroup;
                Login.connection.Send(msg);

                this.Close();
                GroupChat NewGroupChat = new GroupChat(NGroup.ID.ToString());
                UserData.DictionaryGroupChat.Add(NGroup.ID.ToString(), NewGroupChat);
                NewGroupChat.Activate();
                NewGroupChat.Topmost = true;
                NewGroupChat.Show();
            }));
        }

        private void conConfirm_Click(object sender, RoutedEventArgs e)
        {
            if (conMsgTitle.Text.Trim() != "")
            {
                conConfirm.IsEnabled = false;
                Task UpdateTX = new Task(UpdateTask);
                UpdateTX.Start();

            }
            else
            {
                MsgManger.Show("请添加讨论名称");
            }
        }

        private void conCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
