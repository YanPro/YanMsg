﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FirstFloor.ModernUI.Windows.Controls;
using YanMsg.APCode;

namespace YanMsg.View
{
    /// <summary>
    /// Users.xaml 的交互逻辑
    /// </summary>
    public partial class FileTable : ModernWindow
    {

        public FileTable()
        {
          
            InitializeComponent();
            this.Style = (Style)App.Current.Resources["EmptyWindow"];

            SplFileBox.Children.Clear();
            SplFileBox.Children.Add(new FileBox(UserData.UserNumber));
            SplPublicFileBox.Children.Clear();
            string strHasAuthName = "xyb,bjw,wzb,lyh";
            string strIsShow = strHasAuthName.Contains(UserData.UserNumber) ? "N" : "Y";
            SplPublicFileBox.Children.Add(new PublicFileBox("admin", strIsShow));

            
        }

    }
}
