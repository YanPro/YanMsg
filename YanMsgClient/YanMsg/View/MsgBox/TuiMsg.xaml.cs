﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FirstFloor.ModernUI.Windows.Controls;

namespace YanMsg.View
{
    /// <summary>
    /// MsgRead.xaml 的交互逻辑
    /// </summary>
    public partial class TuiMsg : ModernWindow
    {
        public TuiMsg()
        {
           // BBCode=" Supporting [b]bold[/b], [i]italic[/i], [b][i]bold italic[/i][/b], [u]underline[/u], [color=#ff4500]colors[/color], [size=10]different[/size] [size=16]sizes[/size] and support for [url=http://www.163.com]navigable urls[/url].&#13;&#10;&#13;&#10;about link navigation see the [url=/Pages/Navigation.xaml|_top]navigation page[/url]."
            InitializeComponent();
            this.Style = (Style)App.Current.Resources["EmptyWindow"];
        }
    }
}
