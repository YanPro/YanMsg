﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WPFYanChatServer
{
    public delegate void UpdateUserStatusHandler(string strUser, string strUserStuts);

    public class YanChatHelp
    {
        public List<vw_Yan_Chat_user> GetAllUser()
        {
            List<vw_Yan_Chat_user> USERS = new Yan_UserB().GetEntities().ToList();
            return USERS;
        }

        /// <summary>
        /// 获取讨论组的用户
        /// </summary>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        public List<string> GetGroupUsers(string strGroupID)
        {
            var Users = from o in new UG().GetEntities(d => d.UserGroupID == strGroupID)
                         join c in new Yan_UserB().GetEntities() on o.UserID equals c.UserName.ToString()
                         select c.UserName;
            return Users.ToList();
        }

        public static bool CheckLogin(string strUserName,string strPasword)
        {
            List<Yan_User> USERS = new User().GetEntities(" UserName='" + strUserName + "' and UserPass='" + strPasword + "'").ToList();
            return USERS.Count() > 0;

        }



        /// <summary>
        /// 根据ID获取该用户没有接收到的信息
        /// </summary>
        /// <param name="strToUserID"></param>
        /// <returns></returns>
        public List<YanChat_Msg> GetMsgByUser(string strToUserID)
        {
            List<YanChat_Msg> ListNSendMsgs = new YanChat_MsgB().GetEntities(d => d.UserTo == strToUserID && d.IsSend == "N").ToList();
            return ListNSendMsgs;
        }
    }
}